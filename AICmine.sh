#!/bin/bash

#Auto-Installer for Centos Minecraft Server (AICmine)
#AICWoW was made by Nagi from Infinity-WoW team.
#Contact us at http://infinity-wow.net
#or on TeamSpeak3 address: infinity-wow.net
#Enjoy it




#You only need fill these
MYSQLROOTPASS=''    	                                     	 #Password for Mysql root user
TOOLSPATH=''              		                      			 #Path to create Tools directory
DOWNLOADSPATH=''                            		      		 #Path to create Downloads directory


#You dont need to make changes from here

C_YELLOW="\E[33m"
C_BLUE="\E[34m"
C_RED="\E[31m"
C_GREEN="\E[32m"
C_NORMAL="\E[0m"
C_REDBOLD="\E[1;31m"
C_YELLOWBOLD="\E[1;33m"


#Program start
echo -e "${C_YELLOW}Welcome to AICmine"
echo -e "Please edit your settings from AICmine.sh"
echo -e "${C_RED}After use this program, don't run it again."
START=true
start=false
READY=''
while [ "$START" != "$start" ]
do
        echo -e "${C_GREEN}Are you ready? ${C_YELLOWBOLD}y${C_NORMAL}/${C_REDBOLD}n ${C_NORMAL}"; read READY
        if [ "$READY" == "y" ]
        then
                start='true'
        elif [ "$READY" == "n" ]
        then
                start='true'
        fi
done

if [ "$READY" == "y" ]
then
        echo -e "${C_YELLOW}The instalation of all do you need to run a Minecraft High Performance server, and protecctions its about to start."
        sleep 5
	sleep 5
        mkdir $TOOLSPATH
        mkdir $DOWNLOADSPATH
        cd $TOOLSPATH
        yum remove mysql* -y
        rpm -Uhv http://www.percona.com/downloads/percona-release/percona-release-0.0-1.x86_64.rpm
        yum install Percona-Server-client-56 Percona-Server-server-56 Percona-Server-56-debuginfo Percona-Server-devel-56 -y
        yum groupinstall "Development tools" -y
        yum groupinstall "Server Platform Development" -y
        yum groupinstall "Additional Development" -y --skip-broken
        yum install java* -y
	yum install git* wget links zip unzip -y
        service mysql start
        chkconfig --level 2345 mysql on
        mysqladmin -u root password $MYSQLROOTPASS
        yum install vixie-cron -y
        /sbin/chkconfig crond on
        /etc/init.d/crond start
        wget http://pkgs.repoforge.org/unrar/unrar-4.0.7-1.el6.rf.x86_64.rpm
        rpm -Uvh unrar-4.0.7-1.el6.rf.x86_64.rpm
        wget http://pkgs.repoforge.org/rar/rar-3.8.0-1.el6.rf.x86_64.rpm
        rpm -Uvh rar-3.8.0-1.el6.rf.x86_64.rpm
        wget http://www.cmake.org/files/v2.8/cmake-2.8.3.tar.gz
        tar xzf cmake-2.8.3.tar.gz
        cd cmake-2.8.3
        ./configure shared
        make
        make install
	cd $TOOLSPATH
        wget http://download.dre.vanderbilt.edu/previous_versions/ACE-6.0.3.tar.gz
        tar xvzf ACE-6.0.3.tar.gz
        cd ACE_wrappers/
        mkdir build
        cd build
        ../configure --disable-ssl
        make -j32
        make install
	cd $TOOLSPATH
	yum install sendmail -y
        yum install httpd* -y
        chkconfig --levels 235 httpd on
        service httpd start
        yum install percona* --skip-broken -y
        service httpd restart
        yum install readline compat-readline5 compat-readline5-devel compat-readline5-static libreadline-java libreadline-java-javadoc readline-devel readline-static -y
        yum install php* -y
        service httpd restart
        cd /home
	mkdir Ts3
        cd Ts3
        wget http://ftp.4players.de/pub/hosted/ts3/releases/3.0.10.1/teamspeak3-server_linux-amd64-3.0.10.1.tar.gz
        tar xvzf teamspeak3-server_linux-amd64-3.0.10.1.tar.gz
        cd /home
	cd $TOOLSPATH
        service iptables start
        wget http://packages.sw.be/rpmforge-release/rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm
        rpm -Uvh rpmforge-release-0.5.2-2.el6.rf.x86_64.rpm
        wget http://dag.wieers.com/rpm/packages/RPM-GPG-KEY.dag.txt
        rpm --import RPM-GPG-KEY.dag.txt
        yum --enablerepo=rpmforge install aria2 -y
        yum install fail2ban -y
        chkconfig --level 23 fail2ban on
        service fail2ban start
        iptables -L
        sleep 10
		#Add your port of your minecraft servers
		iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 3306 -j ACCEPT                     #Mysql port
        service iptables save
		iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 10011 -j ACCEPT                    #TS3
        service iptables save
        iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 30033 -j ACCEPT                    #TS3
        service iptables save
        iptables -A INPUT -m state --state NEW -m udp -p udp --dport 9987 -j ACCEPT                     #TS3
        service iptables save
        iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 3443 -j ACCEPT                     #RA port
        service iptables save
        iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 7878 -j ACCEPT                     #Soap port
        service iptables save
        iptables -A INPUT -m state --state NEW -m udp -p udp --dport 80 -j ACCEPT
        service iptables save
        iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
        service iptables save
        service fail2ban stop
        service iptables restart
        service fail2ban start
	export VISUAL='nano'
	cat /etc/sysconfig/selinux
	echo -e "${C_GREEN}Add all server port you will need on the Firewall on /etc/sysconfig/iptables and restart 'service iptables restart' ${C_NORMAL} "
	sleep 5
        echo -e "${C_GREEN}If u have issues with php, ports or httpd change on /etc/sysconfig/selinux this line  SELINUX=enforcing to SELINUX=permissive and restart the machine. ${C_NORMAL} "
        sleep 5
         echo -e "${C_GREEN}Congrats!! now you have Minecraft server, compiling tools, web tools, teamspeak, Percona BD and a nice protection versus dos and Ddos attacks ${C_NORMAL} "
         echo -e "${C_YELLOW}Thanks for using this program please visit our server and staff on www.infinity-wow.net or in teamspeak address infinity-wow.net ${C_NORMAL} "
         echo -e "${C_RED}     MADE            BY              ${C_BLUE}NAGI ${C_NORMAL} "
elif [ "$READY" == "n" ]
then
         echo -e "${C_GREEN}Exit sucefull. ${C_NORMAL} "
fi

